package com.server.test.tool;

import java.util.ArrayList;
import java.util.Arrays;

public class ReferencePriceSourceImp implements ReferencePriceSource{
    private ArrayList<Integer> securityIdList = null;
    private double[] priceList = null;
    public ReferencePriceSourceImp(){
        // Assumptions
        this.securityIdList = new ArrayList<>();                // Assumptions: I assume there are 4 different securities.
        this.securityIdList.add(123);
        this.securityIdList.add(124);
        this.securityIdList.add(125);
        this.securityIdList.add(126);
        this.priceList = new double[]{11.2,12.5,31.4,44.9};     // Assumptions: the price of each security
    }


    @Override
    public void subscribe(ReferencePriceSourceListener listener) {
        listener.referencePriceChanged(123,11.4);
    }

    @Override
    public double get(int securityId) {
        int index = this.securityIdList.indexOf(securityId);
        if(index != -1){
            return this.priceList[index];
        }else {
            return -1;
        }

    }
}
