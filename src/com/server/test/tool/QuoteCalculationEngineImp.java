package com.server.test.tool;

public class QuoteCalculationEngineImp implements QuoteCalculationEngine{
    @Override
    public double calculateQuotePrice(int securityId, double referencePrice, boolean buy, int quantity) {
        return referencePrice*quantity;
    }
    public double getResult(String msg){
        String[] res = msg.split(" ");
        int securityId = Integer.parseInt(res[0]);
        boolean buy = false;
        if (res[1].equals("BUY")){
            buy = true;
        }
        int quantity = Integer.parseInt(res[2]);
        ReferencePriceSourceImp rpsi = new ReferencePriceSourceImp();
        double price = rpsi.get(securityId);
        if (price == -1){
            return -1;
        }
        return calculateQuotePrice(securityId,price,buy,quantity);
    }
}
