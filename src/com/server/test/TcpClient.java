package com.server.test;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class TcpClient {
    public static void main(String[] args) {
        try {
            Socket socket = new Socket("127.0.0.1", 10068);
            OutputStream outputStream = socket.getOutputStream();
            OutputStreamWriter writer=new OutputStreamWriter(outputStream,"UTF-8");
            System.out.print("input：");
            Scanner sc = new Scanner(System.in);
            String data = sc.nextLine();
            writer.write(data);
            writer.flush();
            socket.shutdownOutput();

            InputStream inputStream = socket.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream,"UTF-8");
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String info = null;


            System.out.println("client IP address:"+socket.getInetAddress().getHostAddress());
            while ((info = bufferedReader.readLine()) != null) {
                System.out.println("client receive response：" + info);
            }
            //close
            bufferedReader.close();
            inputStreamReader.close();
            inputStream.close();
            writer.close();
            outputStream.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
