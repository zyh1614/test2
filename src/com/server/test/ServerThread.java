package com.server.test;

import com.server.test.tool.QuoteCalculationEngine;
import com.server.test.tool.QuoteCalculationEngineImp;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;

public class ServerThread extends Thread {
    Socket socket = null;
    InetAddress inetAddress=null;

    public ServerThread(Socket socket,InetAddress inetAddress) {
        this.socket = socket;
        this.inetAddress=inetAddress;
    }

    @Override
    public void run() {
        InputStream inputStream = null;
        InputStreamReader inputStreamReader = null;
        BufferedReader bufferedReader = null;
        OutputStream outputStream = null;
        OutputStreamWriter writer = null;
        try {
            inputStream = socket.getInputStream();
            inputStreamReader = new InputStreamReader(inputStream, "UTF-8");
            bufferedReader = new BufferedReader(inputStreamReader);
            String info = null;
            String data = "";

            //循环读取客户端信息
            while ((info = bufferedReader.readLine()) != null) {
                System.out.println("server receive："+"{'from_client ip':'"+socket.getInetAddress().getHostAddress()+"{'from_client port':'"+String.valueOf(socket.getPort())+"','data':'"+info+"'}");
                QuoteCalculationEngineImp qcei = new QuoteCalculationEngineImp();
                double res = qcei.getResult(info);

                if(res == -1){
                    data = "error, return -1";
                }else {
                    data = String.valueOf(res);
                }
            }

            socket.shutdownInput();

            //response
            outputStream = socket.getOutputStream();
            writer = new OutputStreamWriter(outputStream, "UTF-8");
            writer.write(String.valueOf(data));
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            //关闭资源
            try {
                if (writer != null) {
                    writer.close();
                }
                if (outputStream != null) {
                    outputStream.close();
                }
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
                if (inputStreamReader != null) {
                    inputStreamReader.close();
                }
                if (inputStream != null) {
                    inputStream.close();
                }
                if (socket != null) {
                    socket.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}