package com.server.test;


import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class TcpServer {
    public static void main(String[] args) {
        try {
            ServerSocket serverSocket = new ServerSocket(10068);
            Socket socket = null;
            int count = 0;
            System.out.println("server start");
            //loop receive client request
            while (true) {
                socket = serverSocket.accept();
                InetAddress inetAddress=socket.getInetAddress();
                ServerThread thread=new ServerThread(socket,inetAddress);//creat a new thread
                thread.start();
                count++;
                System.out.println("client count：" + count);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}