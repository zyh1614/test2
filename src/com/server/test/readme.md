**Assumptions:**

1.the price of buy and sell are same,because there is only one get method on ReferencePriceSource,also the parameter of get method only has one securityId, so I am not sure if the get method get buy price or sell price.

2.I don't know how many securities there are, so I assume there are 4 different securities.

3.I don't know the price of each security, so I assume it.

**Run:**
1.Run TcpServer
2.Run multiple TcpClient to commucate with the server

**Result:**

server

![img.png](img.png)

client1:

![img_1.png](img_1.png)

client2:

![img_2.png](img_2.png)

client3:

![img_3.png](img_3.png)

client4:

![img_4.png](img_4.png)